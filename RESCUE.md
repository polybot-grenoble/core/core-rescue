# Core Rescue
In case Core Linux or the RPI4 stop working during the Eurobot event.

This system must be able to execute a so-called strategie by sending commands to other peripherals over the CAN bus. It is based on a single STM32L432KC which must replace the RPI4 in no time. **Each step is described in this document.**

Core Rescue shall provides the following features:
- Use the existing Tirette
- Team selection
- State indicator
- Execute a single strategie depending of the team color
- Plug & Play "zero PC" setup

It will not be able to communicate with the superviseur because it requires the RPI4. You need to keep in mind that Core Rescue is a degraded mode, it is designed to collect as many points as possible with simple actions.

## States
The user LED (**LED3** and **D13**) indicates the current state of Core Rescue. States are described in the table below.

|#|LED activity|Description|
|--|--|--|
|-1|Express blink (0,1 second period, 50 duty cycle) |CAN disconnected.|
|0|Off|Yellow team selected.|
|1|On|Blue team selected.|
|2|Slow blink (5 seconds period, 50% duty cycle) |Tirette on. Team selection is frozen.|
|3|Normal blink (1 second period, 50% duty cycle) |Tirette off. Strategie is on.|
|4|Half blink (1 second period, 10% duty cycle) |Strategie end.|

## Step by step setup
1. Unplug the RPI4 from the CAN bus, shut it down but make sure it is still powered
2. Flash Core-Rescue on a spare L432KC equipped with a CAN shield
3. Plug the spare L432KC to the CAN bus and secure it on the robot
4. Plug the spare L432KC to the RPI4 using a micro-usb cable for power. If the RPI4 is not powered up, unplug the cable and plug +5V & GND from the L432KC to the corresponding power supply using Dupont cables. Secure them with tape.
5. Plug one side of the **tirette** on **D2** and the other side on **D3**.
6. Plug one side of a naked tirette switch (with soldered cables and female endings) on **D4** and the other side on **D5**. It will be used as the team selector. When the user LED (**LED3** and **D13**).

## How to use
Once it is set up, you can launch a strategie using the following steps:
1. Put the robot on its starting zone.
2. Reset every single STM32L432KC, ending with the one running Core Rescue.
3. Select the team side using the switch. Triple check the value using the states table and the onboard LED.
4. Put the tirette inside its slot. The strategie will setup. Make sure nobody is touching the robot before inserting the tirette.
5. Pull the tirette at the beginning of the game, the strategie will run.
6. At the end of the game, do not forget to reset the Core Rescue device otherwise you will not be able to start another strategie.
