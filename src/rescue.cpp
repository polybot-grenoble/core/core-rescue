#include "rescue.hpp"

Rescue::Rescue(int pinLed, int pinTirette, int pinTirettePower, int pinSelector,
               int pinSelectorPower, Team defaultTeam) {
    state = SETUP;

    ledPin = pinLed;
    tirettePin = pinTirette;
    selectorPin = pinSelector;

    pinMode(ledPin, OUTPUT);

    pinMode(tirettePin, INPUT_PULLDOWN);
    pinMode(pinTirettePower, OUTPUT);
    pinMode(pinTirettePower, HIGH);

    pinMode(selectorPin, INPUT_PULLDOWN);
    pinMode(pinSelectorPower, OUTPUT);
    pinMode(pinSelectorPower, HIGH);

    tiretteCache = digitalRead(tirettePin);
    selectorCache = digitalRead(selectorPin);
    setupDone = false;
    counter = 0;

    setTeam(team);
};

bool Rescue::setTeam(Team t) {
    if (state != SETUP) return false;
    team = t;
    digitalWrite(ledPin, t);

    return true;
}

Team Rescue::getTeam() { return team; }

void Rescue::update() {
    switch (state) {
        case SETUP:
            if (selectorCache != digitalRead(selectorPin)) {
                selectorCache = digitalRead(selectorPin);
                setTeam((Team)selectorCache);
            }

            if (tiretteCache != digitalRead(tirettePin)) {
                tiretteCache = digitalRead(tirettePin);
                if (tiretteCache == HIGH) {
                    state = TIRETTE_ON;
                }
            }
            break;

        case TIRETTE_ON:
            if (!setupDone) {
                lastTime = millis();
                if (team == YELLOW) {
                    setupYellowStrategie();
                } else {
                    setupBlueStrategie();
                }
            }

            if (tiretteCache != digitalRead(tirettePin)) {
                tiretteCache = digitalRead(tirettePin);
                if (tiretteCache == LOW && setupDone) {
                    state = TIRETTE_OFF;
                }
            }
            break;

        case TIRETTE_OFF:
            lastTime = millis();
            if (team == YELLOW) {
                updateYellowStrategie();
            } else {
                updateBlueStrategie();
            }
            break;

        case STOP:
            break;
    }
}

void Rescue::setupBlueStrategie() {
    switch (counter) {
        case 0:
            Hermes_send(&hermes, 12, 0, false, "");
            counter++;
            break;
        case 1:
            if (millis() - lastTime > 1000) {  // Wait for one second
                Hermes_send(&hermes, 12, 0, false, "");
                counter++;
            }
            setupDone = true;
            break;
    }
}

void Rescue::setupYellowStrategie() {
    switch (counter) {
        case 0:
            Hermes_send(&hermes, 12, 0, false, "");
            counter++;
            break;
        case 1:
            if (millis() - lastTime > 1000) {  // Wait for one second
                Hermes_send(&hermes, 12, 0, false, "");
                counter++;
            }
            setupDone = true;
            break;
    }
}

void Rescue::updateBlueStrategie() {
    switch (counter) {
        case 0:
            Hermes_send(&hermes, 12, 0, false, "");
            counter++;
            break;
        case 1:
            if (millis() - lastTime > 1000) {  // Wait for one second
                Hermes_send(&hermes, 12, 0, false, "");
                counter++;
            }
            setupDone = true;
            break;
    }
}

void Rescue::updateYellowStrategie() {
    switch (counter) {
        case 0:
            Hermes_send(&hermes, 12, 0, false, "");
            counter++;
            break;
        case 1:
            if (millis() - lastTime > 1000) {  // Wait for one second
                Hermes_send(&hermes, 12, 0, false, "");
                counter++;
            }
            setupDone = true;
            break;
    }
}