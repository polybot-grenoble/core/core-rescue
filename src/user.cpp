#include "core/core.hpp"
#include "rescue.hpp"

#define PIN_LED D13
#define PIN_TIRETTE D2
#define PIN_TIRETTTE_POWER D3
#define PIN_SELECTOR D4
#define PIN_SELECTOR_POWER D5

Rescue rescue(PIN_LED, PIN_TIRETTE, PIN_TIRETTTE_POWER, PIN_SELECTOR,
              PIN_SELECTOR_POWER);

void Talos_initialisation() {
    Core_setMaximumLoopFrequency(10);
}

void Talos_loop() {
    /**
     * Looping code. Stopped when in error state.
     */
    rescue.update();
}

void Talos_onError() {
    /**
     * Looping code when in error state.
     */
}

void Core_priorityLoop() {
    /**
     * Looping code no matter what the current state is.
     */
}

void Hermes_onMessage(Hermes_t *hermesInstance, uint8_t sender,
                      uint16_t command, uint32_t isResponse) {
    /**
     * Code to be run every time a message is received over the CAN network.
     */
}