#pragma once

#include "core/core.hpp"

typedef enum { SETUP, TIRETTE_ON, TIRETTE_OFF, STOP } RescueState;
typedef enum { YELLOW = false, BLUE = true } Team;

class Rescue {
   private:
    Team team;
    RescueState state;
    
    int ledPin;
    int tirettePin;
    int selectorPin;

    bool tiretteCache;
    bool selectorCache;

    bool setupDone;

    milli_second lastTime;
    int counter;

   public:
    Rescue(int pinLed, int pinTirette, int pinTirettePower, int pinSelector,
           int pinSelectorPower, Team defaultTeam = YELLOW);
    bool setTeam(Team t);
    Team getTeam();
    void update();
    void setupYellowStrategie();
    void setupBlueStrategie();
    void updateYellowStrategie();
    void updateBlueStrategie();
};